## qssi-user 11 RKQ1.200826.002 V13.0.10.0.RLDCNXM release-keys
- Manufacturer: xiaomi
- Platform: kona
- Codename: psyche
- Brand: Xiaomi
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.200826.002
- Incremental: V13.0.10.0.RLDCNXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: zh-CN
- Screen Density: 440
- Fingerprint: Xiaomi/psyche/psyche:11/RKQ1.200826.002/V13.0.10.0.RLDCNXM:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.200826.002-V13.0.10.0.RLDCNXM-release-keys
- Repo: xiaomi_psyche_dump_31824


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
