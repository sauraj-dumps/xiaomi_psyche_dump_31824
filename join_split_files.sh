#!/bin/bash

cat system/system/priv-app/MiBrowser/MiBrowser.apk.* 2>/dev/null >> system/system/priv-app/MiBrowser/MiBrowser.apk
rm -f system/system/priv-app/MiBrowser/MiBrowser.apk.* 2>/dev/null
cat system/system/priv-app/MIUIMusic/MIUIMusic.apk.* 2>/dev/null >> system/system/priv-app/MIUIMusic/MIUIMusic.apk
rm -f system/system/priv-app/MIUIMusic/MIUIMusic.apk.* 2>/dev/null
cat system/system/priv-app/MIUIGallery/MIUIGallery.apk.* 2>/dev/null >> system/system/priv-app/MIUIGallery/MIUIGallery.apk
rm -f system/system/priv-app/MIUIGallery/MIUIGallery.apk.* 2>/dev/null
cat system/system/priv-app/SecurityCenter/SecurityCenter.apk.* 2>/dev/null >> system/system/priv-app/SecurityCenter/SecurityCenter.apk
rm -f system/system/priv-app/SecurityCenter/SecurityCenter.apk.* 2>/dev/null
cat system/system/priv-app/MIUIVideo/MIUIVideo.apk.* 2>/dev/null >> system/system/priv-app/MIUIVideo/MIUIVideo.apk
rm -f system/system/priv-app/MIUIVideo/MIUIVideo.apk.* 2>/dev/null
cat system/system/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null >> system/system/priv-app/MiuiCamera/MiuiCamera.apk
rm -f system/system/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null
cat system/system/lib64/librelight_only_extraphoto.so.* 2>/dev/null >> system/system/lib64/librelight_only_extraphoto.so
rm -f system/system/lib64/librelight_only_extraphoto.so.* 2>/dev/null
cat system/system/apex/com.android.vndk.current.apex.* 2>/dev/null >> system/system/apex/com.android.vndk.current.apex
rm -f system/system/apex/com.android.vndk.current.apex.* 2>/dev/null
cat system/system/apex/com.android.art.release.apex.* 2>/dev/null >> system/system/apex/com.android.art.release.apex
rm -f system/system/apex/com.android.art.release.apex.* 2>/dev/null
cat system/system/app/SogouInput/SogouInput.apk.* 2>/dev/null >> system/system/app/SogouInput/SogouInput.apk
rm -f system/system/app/SogouInput/SogouInput.apk.* 2>/dev/null
cat system/system/app/MiGameService/MiGameService.apk.* 2>/dev/null >> system/system/app/MiGameService/MiGameService.apk
rm -f system/system/app/MiGameService/MiGameService.apk.* 2>/dev/null
cat system/system/app/VoiceAssist/VoiceAssist.apk.* 2>/dev/null >> system/system/app/VoiceAssist/VoiceAssist.apk
rm -f system/system/app/VoiceAssist/VoiceAssist.apk.* 2>/dev/null
cat system/system/data-app/Email/Email.apk.* 2>/dev/null >> system/system/data-app/Email/Email.apk
rm -f system/system/data-app/Email/Email.apk.* 2>/dev/null
cat system/system/data-app/MIFinance/MIFinance.apk.* 2>/dev/null >> system/system/data-app/MIFinance/MIFinance.apk
rm -f system/system/data-app/MIFinance/MIFinance.apk.* 2>/dev/null
cat system/system/data-app/MiShop/MiShop.apk.* 2>/dev/null >> system/system/data-app/MiShop/MiShop.apk
rm -f system/system/data-app/MiShop/MiShop.apk.* 2>/dev/null
cat system/system/data-app/MIUIYoupin/MIUIYoupin.apk.* 2>/dev/null >> system/system/data-app/MIUIYoupin/MIUIYoupin.apk
rm -f system/system/data-app/MIUIYoupin/MIUIYoupin.apk.* 2>/dev/null
cat system/system/data-app/MIUIVipAccount/MIUIVipAccount.apk.* 2>/dev/null >> system/system/data-app/MIUIVipAccount/MIUIVipAccount.apk
rm -f system/system/data-app/MIUIVipAccount/MIUIVipAccount.apk.* 2>/dev/null
cat system/system/data-app/Youpin/Youpin.apk.* 2>/dev/null >> system/system/data-app/Youpin/Youpin.apk
rm -f system/system/data-app/Youpin/Youpin.apk.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat modem/image/sdx55m/qdsp6sw.mbn.* 2>/dev/null >> modem/image/sdx55m/qdsp6sw.mbn
rm -f modem/image/sdx55m/qdsp6sw.mbn.* 2>/dev/null
cat vendor/lib64/libxmi_high_dynamic_range_cdsp.so.* 2>/dev/null >> vendor/lib64/libxmi_high_dynamic_range_cdsp.so
rm -f vendor/lib64/libxmi_high_dynamic_range_cdsp.so.* 2>/dev/null
cat vendor/lib64/librelight_only.so.* 2>/dev/null >> vendor/lib64/librelight_only.so
rm -f vendor/lib64/librelight_only.so.* 2>/dev/null
cat vendor/data-app/SmartHome/SmartHome.apk.* 2>/dev/null >> vendor/data-app/SmartHome/SmartHome.apk
rm -f vendor/data-app/SmartHome/SmartHome.apk.* 2>/dev/null
cat vendor_bootimg/04_dtbdump_(noDZyURF.dtb.* 2>/dev/null >> vendor_bootimg/04_dtbdump_(noDZyURF.dtb
rm -f vendor_bootimg/04_dtbdump_(noDZyURF.dtb.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
